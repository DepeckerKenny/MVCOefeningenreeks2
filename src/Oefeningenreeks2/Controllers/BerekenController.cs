﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyHowest;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Oefeningenreeks2.Controllers
{
    public class BerekenController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Faculteit(int value)
        {
            ViewBag.value = value;
            ViewBag.facResult = MyHowest.Faculteit.Bereken(value);
            return View();
        }

        public IActionResult Temperatuur(string convertTo, int fromTemp)
        {
            Temperatuur temp = new Temperatuur(fromTemp);
            switch (convertTo) {
                case "inFahrenheit":
                    ViewBag.eenheid = "inFahrenheit";
                    ViewBag.tempResultaat = temp.Fahrenheit;
                    break;
                case "inKelvin":
                    ViewBag.eenheid = "inKelvin";
                    ViewBag.tempResultaat = temp.Kelvin;
                    break;
                default:
                    ViewBag.tempResultaat = fromTemp;
                    break;
            }
            return View();
        }
    }
}
